export const typeScale = {
  authLogo: '36px',
  textInput: '13px',
  buttonText: '17px',
};

export const lineHeight = {
  authLogo: '36px',
  textInput: '22px',
  buttonText: '45px',
};
